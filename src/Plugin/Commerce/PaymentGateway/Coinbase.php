<?php

namespace Drupal\coinbase\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a payment gateway plugin for Coinbase payments.
 *
 * @CommercePaymentGateway(
 *   id = "coinbase_payment",
 *   label = "Coinbase payment",
 *   display_label = "Coinbase payment",
 *   forms = {
 *     "offsite-payment" = "Drupal\coinbase\PluginForm\OffsiteRedirect\CustomPaymentOffsiteForm",
 *   }
 * )
 */
class Coinbase extends OffsitePaymentGatewayBase {

  /**
   * Implemets defaultConfiguration function.
   */
  public function defaultConfiguration() {
    return [
      'coinbase_api_key' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * Implemets buildConfigurationForm function.
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['coinbase_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Clé API'),
      '#default_value' => $this->configuration['coinbase_api_key'],
    ];

    return $form;
  }

  /**
   * Implemets submitConfigurationForm function.
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['coinbase_api_key'] = $values['coinbase_api_key'];
    }
  }

}
