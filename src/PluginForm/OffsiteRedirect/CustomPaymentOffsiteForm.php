<?php

namespace Drupal\coinbase\PluginForm\OffsiteRedirect;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements CustomPaymentOffsiteForm class.
 */
class CustomPaymentOffsiteForm extends BasePaymentOffsiteForm {

  /**
   * Implements buildConfigurationForm function.
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();

    $configuration = $payment_gateway_plugin->getConfiguration();

    $redirect_method = 'post';

    $data = [
      'amount' => $payment->getAmount()->getNumber(),
      'currency' => $payment->getAmount()->getCurrencyCode(),
      'pricing_type' => 'fixed_price',
      'return' => $form['#return_url'],
      'cancel' => $form['#cancel_url'],
    ];

    $curl = curl_init();
    curl_setopt_array($curl, [
      CURLOPT_URL => 'https://api.commerce.coinbase.com/charges/',
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => TRUE,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => '{"local_price":{"amount":"' . $data['amount'] . '",
                "currency":"' . $data['currency'] . '"},"pricing_type":"' . $data['pricing_type'] . '"}',
      CURLOPT_HTTPHEADER => [
        'Content-Type: application/json',
        'Accept: application/json',
        'X-CC-Api-Key: ' . $configuration['coinbase_api_key'],
      ],
    ]);

    $response = curl_exec($curl);

    $res = json_decode($response, TRUE);
    $redirect_url = $res['data']['hosted_url'];

    $form = $this->buildRedirectForm($form, $form_state, $redirect_url, $data, $redirect_method);

    return $form;
  }

}
